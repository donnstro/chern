# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7
inherit font gnome2-utils eutils multilib unpacker

DESCRIPTION="СБИС Плагин. Desktop Plugin for convenient work in the browser SBIS3Plugin (SABY Plugin) — a desktop application for working with notifications, editing documents in a browser, entering your personal account by certificate, uploading files to the cloud, and so on"
HOMEPAGE="https://sbis.ru/"

KEYWORDS="amd64"

SRC_URI="amd64? ( https://update-msk2.sbis.ru/Sbis3Plugin/master/linux/sbis3plugin.deb -> sbis3plugin-${PV}.deb )"

SLOT="0"
RESTRICT="strip mirror"
LICENSE="GPL-3"
IUSE=""

NATIVE_DEPEND="app-crypt/sbis3plugin-core"
RDEPEND="${NATIVE_DEPEND}"
DEPEND="${RDEPEND}"

S="${WORKDIR}"

src_install() {
    cp -R "${WORKDIR}/opt" "${D}" || die "install failed!"
    cp -R "${FILESDIR}/sbis3plugin-postinst.sh" "${D}/opt/sbis3plugin/sbis3plugin-postinst.sh" || die "install failed!"
}

pkg_postinst() {
    cp -R /opt/sbis3plugin/temp/* /opt/sbis3plugin/
    /etc/init.d/SBIS3Plugin-daemon stop
    cp -R "${FILESDIR}/SBIS3Plugin-daemon" "/etc/init.d/SBIS3Plugin-daemon" || die "install failed!"
    mkdir -p /var/run/sbis
    elog "Установка происходит в автоматическом режиме."
    elog "При возникновении проблем с запуском, используйте сценарий ниже:"
    elog "1. Настройте версию sbis3plugin в файле /opt/sbis3plugin/sbis3plugin-postinst.sh в строке VERSBIS=<устанавливаемая версия>"
    elog "2. Исполните файл сценария /opt/sbis3plugin/sbis3plugin-postinst.sh"
    elog "3. Пропишите автозапуск rc-update add SBIS3Plugin-daemon default"
    elog "4. Запустите /etc/init.d/SBIS3Plugin-daemon start"
    elog "5. Повторите исполнение сценария /opt/sbis3plugin/sbis3plugin-postinst.sh"
    elog "6. Запустите приложение самостоятельно с помощью ярлыка на рабочем столе."
    /bin/bash /opt/sbis3plugin/sbis3plugin-postinst.sh
    rc-update add SBIS3Plugin-daemon default
    /etc/init.d/SBIS3Plugin-daemon start
    /bin/bash /opt/sbis3plugin/sbis3plugin-postinst.sh
    rm -R /opt/sbis3plugin/temp
    rm -R /opt/sbis3plugin/temp_minimal_core
}
