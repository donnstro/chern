# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit rpm

DESCRIPTION="Application CryptoARM GOST is designed to create a digital signature and file encryption using digital certificates and cryptographic algorithms."
HOMEPAGE="https://cryptoarm.ru/"
SRC_URI="https://github.com/CryptoARM/CryptoARMGOST/releases/download/v2.5.12/cryptoarm-gost-v2.5.12-linux-x64.rpm"

LICENSE=""
SLOT="0"
KEYWORDS="amd64"

S=${WORKDIR}
MY_P="cryptoarm-gost"

src_install() {
	cp -vR ${S}/* ${D}/
# Исправление ярлыка для меню Пуск
	sed -i -E -e 's/cryptoarm-gost %u/cryptoarm-gost %u --no-sandbox/' ${D}/usr/share/applications/cryptoarm-gost.desktop
}

pkg_postinst() {
	"${D}"/opt/cryptoarm_gost/ssl/addCA.linux.sh
}

pkg_prerm ()  {

    rm -Rv /opt/cryptoarm_gost/
    rm -Rv /usr/share/applications/cryptoarm-gost.desktop
    rm -Rv /usr/share/pixmaps/cryptoarm-gost.png

}
