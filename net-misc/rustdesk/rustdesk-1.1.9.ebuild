# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit desktop xdg unpacker

DESCRIPTION="Open source virtual / remote desktop infrastructure for everyone!"
HOMEPAGE="https://rustdesk.com/"
SRC_URI="https://github.com/rustdesk/rustdesk/releases/download/1.1.9/rustdesk-1.1.9.deb -> ${P}.deb"

LICENSE=""
SLOT="0"
KEYWORDS="amd64"

# Зависимости для запуска этой программы
RDEPEND="
	x11-libs/libxcb
	x11-libs/gtk+:3
	x11-misc/xdotool
	x11-libs/libXfixes
	media-sound/pulseaudio
	dev-lang/python
	net-misc/curl
"
S=${WORKDIR}
MY_P="rustdesk"
OPT="usr/${MY_P}"

src_unpack() {
	unpack_deb ${A}
}

src_install() {
	cp -R usr "${D}"
}

pkg_postinst() {
	# Добавление службы rustdesk
	echo '#!/sbin/openrc-run

name=$RC_SVCNAME
description="RustDesk Daemon Service"
supervisor="supervise-daemon"
command="/usr/bin/rustdesk"
command_args="--service"

depend() {
	after xdm
	need net
}' > /etc/init.d/rustdesk
	# Исправление прав на запуск службы
	chmod +x /etc/init.d/rustdesk
	# Добавление службы в автозагрузку
	rc-update add rustdesk default
	# Запуск службы rustdesk
	/etc/init.d/rustdesk start
}

pkg_postrm() {
	# Зачистка мусора
	rc-update delete rustdesk
	/etc/init.d/rustdesk stop

	# Удалить службу
	rm -f /etc/init.d/rustdesk || die
}
